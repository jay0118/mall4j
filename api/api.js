import {
	request
} from "@/utils/httprequest.js"
const getProdListByTagId = (data) => {
	request({
		url: "prod/prodListByTagId",
		data,
		method: "GET",
	})
}
module.exports = {
	getProdListByTagId
}
