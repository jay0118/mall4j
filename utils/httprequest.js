const proxy = require("./proxyUrl")

const request = ({
	url,
	data,
	method
} => {
	uni.showLoading({
		title: "加载中。。。"
	})
	return new Promise((resolve, reject) => {
		uni.request({
			url: proxy + url,
			data,
			method,
			header: {},
			success: (res) => {
				switch (res.statusCode) {
					case 200:
						setTimeout(() => {
							resolve(res.data)
							uni.hideLoading()
						}, 1000)
						break;
					case 401:
						uni.showToast({
							title: "您没有访问权限！"
						})
						break;
					case 500:
						uni.showToast({
							title: "服务器开了点小差~"
						})
						break;
					default:
						break;
				}
			},
			fail: (error) => {
				reject(error)
				uni.hideLoading();
				setTimeout((
					uni.showToast({
						title: "请求失败",
						icon: "error",
					});
				) => , 1500)
			}

		})
	})
})
module.exports = {
	request
}
